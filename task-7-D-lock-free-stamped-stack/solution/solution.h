#pragma once

#include "arena_allocator.h"

#include <atomic>

// to prevent compiler warnings in solution template
#define UNUSED(x) (void)(x)

template<typename T, template <typename U> class Atomic = std::atomic>
class LockFreeStack {
    struct Node {
        T data_;
        ArenaRef next_;

        Node(T data, ArenaRef next = ARENA_NULL_REF)
            : data_(data)
            , next_(next) {
        }
    };

public:
    LockFreeStack(const size_t capacity)
        : arena_(capacity) {
    }

    void Push(T data) {
        // usage example
        ArenaRef node_ref = GetNodeForPush(data);
        PushToStack(stack_top_, node_ref);
    }

    bool Pop(T& data) {
         return false; // not implemented
    }

private:
    ArenaRef GetNodeForPush(T data) {
        // node recycling:
        // reuse node if free pool is not empty or allocate new node in arena otherwise

        return arena_.New(data); // not implemented
    }

    void ReleaseNode(ArenaRef node_ref) {
        UNUSED(node_ref);
        // not implemented
    }

    bool TryReuseNode(ArenaRef& node_ref) {
        UNUSED(node_ref);
        return false; // not implemented
    }

    void PushToStack(Atomic<ArenaStampedRef>& top, ArenaRef new_node_ref) {
        UNUSED(top);
        UNUSED(new_node_ref);
        // not implemented
    }

    bool PopFromStack(Atomic<ArenaStampedRef>& top, ArenaRef& popped_node_ref) {
        UNUSED(top);
        UNUSED(popped_node_ref);
        return false; // not implemented
    }

private:
    Arena<Node> arena_;
    Atomic<ArenaStampedRef> stack_top_{ARENA_NULL_REF};
    Atomic<ArenaStampedRef> free_pool_top_{ARENA_NULL_REF};
};


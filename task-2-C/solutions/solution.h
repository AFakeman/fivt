#pragma once

#include <cstddef>

class Robot {
public:
    Robot(const std::size_t num_foots);

    void Step(const std::size_t foot);
};

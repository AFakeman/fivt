#pragma once

#include "arena_allocator.h"

#include <atomic>
#include <limits>

///////////////////////////////////////////////////////////////////////

template <typename T>
struct ElementTraits {
    static T Min() {
        return std::numeric_limits<T>::min();
    }
    static T Max() {
        return std::numeric_limits<T>::max();
    }
};

///////////////////////////////////////////////////////////////////////

class SpinLock {
 public:
    explicit SpinLock() {
        // not implemented
    }

    void Lock() {
        // not implemented
    }

    void Unlock() {
        // not implemented
    }


    // adapters for BasicLockable concept

    void lock() {
        Lock();
    }

    void unlock() {
        Unlock();
    }
};

///////////////////////////////////////////////////////////////////////

template <typename T>
class OptimisticLinkedSet {
 private:
    struct Node {
        T element_;
        std::atomic<Node*> next_;
        SpinLock lock_{};
        std::atomic<bool> marked_{false};

        Node(const T& element, Node* next = nullptr)
            : element_(element),
              next_(next) {
        }
    };

    struct Edge {
        Node* pred_;
        Node* curr_;

        Edge(Node* pred, Node* curr)
            : pred_(pred),
              curr_(curr) {
        }
    };

 public:
    explicit OptimisticLinkedSet(ArenaAllocator& allocator)
        : allocator_(allocator) {
        CreateEmptyList();
    }

    bool Insert(const T& element) {
        return false; // not implemented
    }

    bool Remove(const T& element) {
        return false; // not implemented
    }

    bool Contains(const T& element) const {
        return false; // not implemented
    }

    size_t Size() const {
        return 0; // not implemented
    }

 private:
    void CreateEmptyList() {
        head_ = allocator_.New<Node>(ElementTraits<T>::Min());
        head_->next_ = allocator_.New<Node>(ElementTraits<T>::Max());
    }

    Edge Locate(const T& /* element */) const {
        return Edge{nullptr, nullptr}; // not implemented
    }

    bool Validate(const Edge& /* edge */) const {
        return false; // not implemented
    }

 private:
    ArenaAllocator& allocator_;
    Node* head_{nullptr};
};

template <typename T> using ConcurrentSet = OptimisticLinkedSet<T>;

///////////////////////////////////////////////////////////////////////

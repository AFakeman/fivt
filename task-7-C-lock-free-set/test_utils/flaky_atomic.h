#pragma once

#include <atomic>
#include <thread>

//////////////////////////////////////////////////////////////////////

// switch threads before/after CAS/exchange operations

template <typename T, size_t YieldFreq = 3>
class FlakyAtomic {
public:
    FlakyAtomic(const T& initial_value)
        : register_(initial_value) {
    }

    T load() const {
        return register_.load();
    }

    void store(const T& value) {
        register_.store(value);
    }

    FlakyAtomic& operator =(const T& value) {
        store(value);
        return *this;
    }

    T exchange(const T& new_value) {
        Yield();
        const T prev_value = register_.exchange(new_value);
        Yield();
        return prev_value;
    }

    bool compare_exchange_weak(T& expected, const T& desired) {
        Yield();
        const bool succeeded = register_.compare_exchange_weak(expected, desired);
        Yield();
        return succeeded;
    }

    bool compare_exchange_strong(T& expected, const T& desired) {
        Yield();
        const bool succeeded = register_.compare_exchange_strong(expected, desired);
        Yield();
        return succeeded;
    }

private:
    void Yield() const {
        static std::atomic<size_t> called{0};

        if (called.fetch_add(1) % YieldFreq == 0) {
            std::this_thread::yield();
        }
    }

private:
    std::atomic<T> register_;
};

//////////////////////////////////////////////////////////////////////

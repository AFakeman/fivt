#pragma once

#include <condition_variable>
#include <mutex>

///////////////////////////////////////////////////////////////////////

class Barrier {
public:
    explicit Barrier(const size_t num_threads)
        : num_threads_{num_threads}
        , thread_count_{num_threads} {
    }

    void Pass() {
        std::unique_lock<std::mutex> lock{mutex_};
        const size_t curr_epoch = epoch_;
        --thread_count_;
        if (thread_count_ == 0) {
            ++epoch_;
            thread_count_ = num_threads_;
            all_threads_arrived_.notify_all();
        } else {
            all_threads_arrived_.wait(lock, [this, curr_epoch]() { return curr_epoch < epoch_; });
        }
    }

private:
    size_t num_threads_;
    std::mutex mutex_;
    std::condition_variable all_threads_arrived_;
    size_t epoch_{0};
    size_t thread_count_;
};

///////////////////////////////////////////////////////////////////////

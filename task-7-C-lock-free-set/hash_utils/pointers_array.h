#pragma once

#include <atomic>
#include <cassert>
#include <cmath>
#include <iostream>

//////////////////////////////////////////////////////////////////////

template <typename T>
class PointersArray {
    using Pointer = std::atomic<T*>;
    using Pointers = Pointer*;

public:
    PointersArray(const size_t size)
        : size_(size),
          block_size_(GetBlockSize(size)) {
        blocks_ = new std::atomic<Pointers>[block_size_]();
    }

    ~PointersArray() {
        for (size_t i = 0; i < block_size_; ++i) {
            if (blocks_[i]) {
                delete[] blocks_[i].load();
            }
        }
        delete[] blocks_;
    }

    bool TrySet(const size_t index, T* ptr) {
        auto& ptr_slot = Access(index);
        T* null_ptr{nullptr};
        return ptr_slot.compare_exchange_strong(null_ptr, ptr);
    }

    T* Get(const size_t index) {
        auto& ptr_slot = Access(index);
        return ptr_slot.load();
    }

private:
    static size_t GetBlockSize(const size_t size) {
        return static_cast<size_t>(sqrt(size) + 1);
    }

    Pointer& Access(const size_t index) {
        assert(index < size_);

        const size_t block_index = index / block_size_;
        const size_t index_in_block = index % block_size_;

        if (!blocks_[block_index]) {
            Pointers new_block = new Pointer[block_size_](); // initialize with zeroes
            Pointers null_ptr{nullptr};
            if (!blocks_[block_index].compare_exchange_strong(null_ptr, new_block)) {
                delete[] new_block;
            }
        }

        return blocks_[block_index][index_in_block];
    }

private:
    size_t size_;
    size_t block_size_;
    std::atomic<Pointers>* blocks_{nullptr};
};

//////////////////////////////////////////////////////////////////////

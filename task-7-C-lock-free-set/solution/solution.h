#pragma once

#include "atomic_marked_pointer.h"
#include "arena_allocator.h"

#include <atomic>
#include <limits>

///////////////////////////////////////////////////////////////////////

template <typename T>
struct ElementTraits {
    static T Min() {
        return std::numeric_limits<T>::min();
    }
    static T Max() {
        return std::numeric_limits<T>::max();
    }
};

///////////////////////////////////////////////////////////////////////

template <typename Element>
class LockFreeLinkedSet {
 private:
    struct Node {
        Element element_;
        AtomicMarkedPointer<Node> next_;

        Node(const Element& element, Node* next = nullptr)
            : element_{element},
              next_{next} {
        }

        bool Marked() const {
            return next_.Marked();
        }

        Node* NextPointer() const {
            return next_.LoadPointer();
        }
    };

    struct Edge {
        Node* pred_;
        Node* curr_;

        Edge(Node* pred, Node* curr)
            : pred_(pred),
              curr_(curr) {
        }
    };

 public:
    explicit LockFreeLinkedSet(ArenaAllocator& allocator)
        : allocator_(allocator) {
        CreateEmptyList();
    }

    bool Insert(const Element& /*element*/) {
        return false; // not implemented
    }

    bool Remove(const Element& /*element*/) {
        return false; // not implemented
    }

    bool Contains(const Element& /*element*/) const {
        return false; // not implemented
    }

    size_t Size() const {
        return 0; // not implemented
    }

 private:
    void CreateEmptyList() {
        head_ = allocator_.New<Node>(ElementTraits<Element>::Min());
        head_->next_.Store(allocator_.New<Node>(std::numeric_limits<Element>::max()));
    }

    Edge Locate(const Element& /* element */) const {
        return {nullptr, nullptr}; // not implemented
    }

 private:
    ArenaAllocator& allocator_;
    Node* head_;
};

///////////////////////////////////////////////////////////////////////

template <typename T> using ConcurrentSet = LockFreeLinkedSet<T>;

///////////////////////////////////////////////////////////////////////

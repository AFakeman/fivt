#include "asserts.h"
#include "barrier.h"
#include "executor.h"
#include "program_options.h"
//#include "ya_contest_sim.h"

//#include "lock_free_stack.h"
#include "solution.h"

#include <atomic>
#include <string>
#include <sstream>
#include <vector>
#include <thread>

//////////////////////////////////////////////////////////////////////

struct TestElement {
    TestElement(const size_t thread_index, const size_t index)
        : thread_index_(thread_index)
        , index_(index)
        , payload_(std::to_string(index)) {
    }

    TestElement() {
    }

    size_t thread_index_{0};
    size_t index_{0};
    std::string payload_{};
};

//////////////////////////////////////////////////////////////////////

class ConcurrentStackTester {
public:
    ConcurrentStackTester(const size_t num_inserts, const size_t num_threads)
        : num_inserts_{num_inserts},
          num_threads_{num_threads},

          push_barrier_{num_threads},
          pop_barrier_{num_threads} {
    }

    void operator ()() {
        RunTest();
    }

private:
    void RunTest() {
        RunThreads();
        ValidateFinalInvariants();
    }

    void RunThreads() {
        TaskExecutor executor;
        for (size_t t = 0; t < num_threads_; ++t) {
            executor.Execute([this, t]() {
                RunContenderThread(t);
            });
        }
    }

    void RunContenderThread(const size_t thread_index) {
        push_barrier_.Pass();

        for (size_t i = thread_index; i < num_inserts_; i += num_threads_) {
            TestElement element{thread_index, i};
            stack_.Push(std::move(element));
        }

        pop_barrier_.Pass();

        size_t consumed_sum = 0;
        std::vector<size_t> prev_indices(num_threads_, (size_t)-1);

        for (size_t i = thread_index; i < num_inserts_; i += num_threads_) {
            TestElement element{};
            test_assert(stack_.Pop(element), "non-empty stack expected");

            const size_t prev_element_index = prev_indices[element.thread_index_];
            const size_t curr_element_index = element.index_;

            if (prev_element_index != (size_t)-1) {
                test_assert(curr_element_index < prev_element_index, "LIFO order violated");
            }

            prev_indices[element.thread_index_] = curr_element_index;

            consumed_sum += element.index_;
        }

        total_consumed_sum_.fetch_add(consumed_sum);
    }

    void ValidateFinalInvariants() {
        TestElement element{};
        test_assert(!stack_.Pop(element), "empty stack expected");

        stack_.Push(TestElement{});

        const size_t expected_sum = num_inserts_ * (num_inserts_ - 1) / 2;
        test_assert(total_consumed_sum_.load() == expected_sum, "unexpected total consumed sum");
    }

private:
    size_t num_inserts_;
    size_t num_threads_;

    OnePassBarrier push_barrier_;
    OnePassBarrier pop_barrier_;

    ConcurrentStack<TestElement> stack_;

    std::atomic<size_t> total_consumed_sum_{0};
};

///////////////////////////////////////////////////////////////////////

void RunTest(int argc, char* argv[]) {
    size_t num_inserts;
    size_t num_threads;

    read_opts(argc, argv, num_inserts, num_threads);

    //SolutionTests::SimulateYandexContest();

    ConcurrentStackTester{num_inserts, num_threads}();
}

int main(int argc, char* argv[]) {
    RunTest(argc, argv);
    return EXIT_SUCCESS;
}

#pragma once

#include <atomic>
#include <thread>

///////////////////////////////////////////////////////////////////////

template <typename T>
class LockFreeStack {
    struct Node {
        T element;
        // to be continued
    };

 public:
    LockFreeStack() {
    }

    ~LockFreeStack() {
        // not implemented
    }

    void Push(T /* element */) {
        // not implemented
    }

    bool Pop(T& /* element */) {
        // not implemented
        return false;
    }

 private:
    std::atomic<Node*> top_{nullptr};
};

///////////////////////////////////////////////////////////////////////

template <typename T>
using ConcurrentStack = LockFreeStack<T>;

///////////////////////////////////////////////////////////////////////

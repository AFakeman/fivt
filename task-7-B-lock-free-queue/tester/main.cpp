#include "asserts.h"
#include "barrier.h"
#include "executor.h"
#include "flaky_atomic.h"
#include "program_options.h"
#include "ya_contest_sim.h"

//#include "lock_free_queue.h"
#include "solution.h"

#include <atomic>
#include <random>
#include <sstream>
#include <string>
#include <thread>
#include <vector>

//////////////////////////////////////////////////////////////////////

struct TestElement {
    TestElement(const size_t thread_index, const size_t index)
        : thread_index_(thread_index)
        , index_(index)
        , payload_(std::to_string(index)) {
    }

    TestElement() {
    }

    size_t thread_index_{0};
    size_t index_{0};
    std::string payload_{};
};

//////////////////////////////////////////////////////////////////////

class EnqueueDequeueBatchTester {
public:
    EnqueueDequeueBatchTester(const size_t num_inserts, const size_t num_threads)
        : num_inserts_{num_inserts},
          num_threads_{num_threads},

          start_barrier_{num_threads} {
    }

    void operator ()() {
        RunTest();
    }

private:
    void RunTest() {
        RunThreads();
        ValidateFinalInvariants();
    }

    void RunThreads() {
        TaskExecutor executor;
        for (size_t t = 0; t < num_threads_; ++t) {
            executor.Execute([this, t]() {
                RunContenderThread(t);
            });
        }
    }

    void RunContenderThread(const size_t thread_index) {
        size_t consumed_sum = 0;
        std::vector<size_t> prev_indices(num_threads_, (size_t)-1);

        std::mt19937 random_generator;
        std::uniform_int_distribution<> next_int(1, 50);

        start_barrier_.Pass();

        size_t i = thread_index;
        while (i < num_inserts_) {
            const size_t batch_size = next_int(random_generator);

            size_t inserted = 0;
            size_t j = 0;

            while (j < batch_size && i < num_inserts_) {
                ++inserted;

                TestElement element(thread_index, i);
                queue_.Enqueue(element);

                ++j;
                i += num_threads_;
            }

            for (size_t k = 0; k < inserted; ++k) {
                TestElement element{};
                test_assert(queue_.Dequeue(element), "non-empty queue expected");

                const size_t prev_element_index = prev_indices[element.thread_index_];
                const size_t curr_element_index = element.index_;

                if (prev_element_index != (size_t)-1) {
                    test_assert(curr_element_index > prev_element_index, "FIFO order violated");
                }

                prev_indices[element.thread_index_] = curr_element_index;

                consumed_sum += element.index_;
            }
        }

        total_consumed_sum_.fetch_add(consumed_sum);
    }

    void ValidateFinalInvariants() {
        TestElement element{};
        test_assert(!queue_.Dequeue(element), "empty queue expected");

        queue_.Enqueue(TestElement{});

        const size_t expected_sum = num_inserts_ * (num_inserts_ - 1) / 2;
        test_assert(total_consumed_sum_.load() == expected_sum, "unexpected total consumed sum");
    }

private:
    size_t num_inserts_;
    size_t num_threads_;

    OnePassBarrier start_barrier_;

    template <typename T> using Atomic = FlakyAtomic<T, 3>;

    LockFreeQueue<TestElement, Atomic> queue_;

    std::atomic<size_t> total_consumed_sum_{0};
};

//////////////////////////////////////////////////////////////////////

class InterleavingDequeuesTester {
public:
    InterleavingDequeuesTester(const size_t num_inserts, const size_t num_threads)
        : num_inserts_{num_inserts},
          num_threads_{num_threads},

          barrier_{num_threads} {
    }

    void operator ()() {
        RunTest();
    }

private:
    void RunTest() {
        TaskExecutor executor;
        for (size_t t = 0; t < num_threads_; ++t) {
            executor.Execute([this, t]() {
                RunContenderThread(t);
            });
        }
    }

    void DoMadSwitches(size_t seed) {
        const size_t switches = (seed * seed) % 17;
        for (size_t i = 0; i < switches; ++i) {
            std::this_thread::yield();
        }
    }

    void RunContenderThread(const size_t thread_index) {
        if (thread_index == 0) {
            for (size_t i = 0; i < num_inserts_; ++i) {
                queue_.Enqueue(TestElement(0, i));
            }
        }

        barrier_.Pass();

        for (size_t i = thread_index; i < num_inserts_; i += num_threads_) {
            TestElement e;
            queue_.Dequeue(e);
            DoMadSwitches(i);
        }
    }

private:
    size_t num_inserts_;
    size_t num_threads_;

    OnePassBarrier barrier_;

    template <typename T> using Atomic = FlakyAtomic<T, 10>;
    LockFreeQueue<TestElement, Atomic> queue_;
};

///////////////////////////////////////////////////////////////////////

void RunStressTests(int argc, char* argv[]) {
    size_t num_inserts;
    size_t num_threads;

    read_opts(argc, argv, num_inserts, num_threads);

    // use in local tests!
    // AttachThreadsToSingleCore();

    EnqueueDequeueBatchTester{num_inserts, num_threads}();
    InterleavingDequeuesTester{num_inserts, num_threads}();
}

int main(int argc, char* argv[]) {
    RunStressTests(argc, argv);
    return EXIT_SUCCESS;
}

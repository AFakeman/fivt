#!/usr/bin/env python3

def output_test(test_num, test_data):
    with open("{test_num:02d}.in".format(test_num=test_num), "w") as f:
        print(test_data, file=f)

def generate_tests():
    for threads, inserts in [
        (100000, 1),
        (100000, 2),
        (100000, 3),
        (100000, 4),
        (100000, 5),
        (100000, 6),
        (100000, 7),
        (100000, 8),
        (100000, 9),
        (100000, 10),
        (90000, 11),
        (80000, 12),
        (70000, 13),
        (60000, 14),
        (50000, 15),
        (50000, 20),
        (50000, 20),
        (50000, 20),
    ]:
        yield "{threads} {inserts}".format(**locals())

def main():
    for test_num, test_data in enumerate(generate_tests()):
        output_test(test_num, test_data)

main()

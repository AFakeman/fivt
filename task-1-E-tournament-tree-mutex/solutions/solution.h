#pragma once

#include <cstddef>

class TournamentTreeMutex {
 public:
    explicit TournamentTreeMutex(std::size_t /* num_threads */) {
    }

    void Lock(std::size_t /* current_thread */) {
    }

    void Unlock(std::size_t /* current_thread */) {
    }
};


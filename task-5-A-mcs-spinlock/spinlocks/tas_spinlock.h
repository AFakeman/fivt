#pragma once

#include "spinlock_pause.h"

#include <atomic>
#include <mutex>

/////////////////////////////////////////////////////////////////////

// Test-And-Set spinlock
template <template <typename T> class Atomic = std::atomic>
class TASSpinLock {
public:
    void Lock() {
        while (locked_.exchange(true)) {
            SpinLockPause();
        }
    }

    void Unlock() {
        locked_.store(false);
    }

    // adapters for BasicLockable concept

    void lock() {
        Lock();
    }

    void unlock() {
        Unlock();
    }

    // guard class

    using Guard = std::lock_guard<TASSpinLock>;

private:
    Atomic<bool> locked_{false};
};

/////////////////////////////////////////////////////////////////////

// alias for tester
template <template <typename T> class Atomic = std::atomic>
using SpinLock = TASSpinLock<Atomic>;

/////////////////////////////////////////////////////////////////////
